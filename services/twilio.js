const config = require('../config');
const twilio = require('twilio');
const twilioClient = new twilio(config.twilio.accountSid, config.twilio.authToken);

const sendText = async (from, to, body, mediaUrl) => {
  return twilioClient.messages.create({
    from: from,
    to: to,
    body: body,
    mediaUrl: mediaUrl
  });
};

const createTwimlResponse = () => {
  return new twilio.twiml.MessagingResponse();
};

module.exports = {
  sendText: sendText,
  createTwimlResponse: createTwimlResponse
}
