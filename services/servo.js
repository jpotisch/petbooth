const Gpio = require('pigpio').Gpio;
const motor = new Gpio(5, {mode: Gpio.OUTPUT});

const { snooze, log } = require('./utils.js');

const raw = (frequency) => {
  motor.servoWrite(frequency);
  log(`Servo set to ${frequency}`);
}
const stop = () => {
  motor.servoWrite(0);
  log('Servo stopped');
};

const start = (direction, speed) => {
  motor.servoWrite(getServoFrequency(direction, speed));
  log('Servo started');
};

const getServoFrequency = (direction, speed) => {
  direction = parseInt(direction);
  speed = parseInt(speed);
  if(isNaN(direction) || Math.pow(direction, 2) != 1) {
    throw 'Direction must be -1 (servo.direction.counterClockwise) or 1 (servo.direction.clockwise)';
  }
  if(isNaN(speed) || speed < 0 || speed > 10) {
    throw 'Speed must be between 0 and 10';
  }
  const servoFrequency = 1500 + direction * speed * 50;
  log(`getServoFrequency(${direction}, ${speed}) = ${servoFrequency}`);
  return servoFrequency;
}

const moveFor = async (direction, speed, durationMs) => {
  start(direction, speed);
  await snooze(durationMs);
  stop();
};

const terminate = () => {
  stop();
  log('Calling pigpio.terminate()');
  require('pigpio').terminate();
}

module.exports = {
  getServoFrequency: getServoFrequency,
  raw: raw,
  stop: stop,
  start: start,
  moveFor: moveFor,
  terminate: terminate,
  direction: {
    counterClockwise: -1,
    clockwise: 1
  }
}
