echo Stopping petbooth . . .
if [ "$HOSTNAME" = petbooth ]; then
    sudo systemctl stop petbooth
else
    ssh petbooth sudo systemctl stop petbooth
fi
echo Done
