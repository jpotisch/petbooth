echo Restarting petbooth . . .
if [ "$HOSTNAME" = petbooth ]; then
    sudo systemctl restart petbooth
else
    ssh petbooth sudo systemctl restart petbooth
fi
echo Done
